@extends('admin.master')
@section('content')

{{ csrf_field() }} 
<h1 class="text-center mb-4 mt-2">Blogs</h1>
@if (session('message'))
<div class="alert alert-success alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ session('message') }}</strong>
</div>
@endif
<div class="row">
  <div class="col-3"><div class="row  d-flex justify-content-center">
  @include('admin.sidebar')
  </div>
  <div class="col-9">
    <div id="msg"></div>
<button name="add" type="buttn" class="btn btn-success btn-sm mb-5" id="add_data">ADD</button>
<table  class="table table-bordered DataTable" style="width:100%" id="blog_table">

<tr>
<thead>
<th>Title</th>
<th>Description</th>
<th>image</th>
<th>order</th>
<th>active</th>
<th>views</th>
<th>Category Name</th>
<th>Auther Name</th>
<th>Actions</th>
</thead>
</tr>






</table>
</div>
</div>
</div>
<div id="dataModel" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<form action="" method="post" id="data_form" enctype="multipart/form-data">

<div class="modal-header">

<button type="button" class="close" data-dismis="modal">&times;</button>
<h4 class="modal-title">Add Data</h4>


</div>
<div class="modal-body">@csrf
<div class="form-group ">
    <label>Title</label>
    <input type="text" class="form-control"  aria-describedby="emailHelp" placeholder="Enter the title" name="title" id="title">
    @error('title')
    <div class="mt-4 mb-4"><span class="alert alert-danger">{{$message}}</span></div>
    @enderror
    
  </div>
  <div class="form-group">
    <label>Description</label>
    <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="Enter discription" name="description" id="description"> 
    @error('description')
    <div class="mt-4 mb-4"><span class="alert alert-danger">{{$message}}</span></div>
    @enderror
  </div>
  <div class="form-group">
  <div class="custom-file">
    <input type="file" name="image" id="image" class="custom-file-input"> 
    <div id="image-holder" class="row mt-2 justify-content-center"> </div>
    @error('image')
    <div class="mt-4 mb-4"><span class="alert alert-danger">{{$message}}</span></div>
    @enderror
    <label class="custom-file-label">Image</label>
    </div>
  </div>
  <div class="form-group">
    <label>Order</label>
    <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="Enter order" name="order" id="order">
    @error('order')
    <div class="mt-4 mb-4"><span class="alert alert-danger">{{$message}}</span></div>
    @enderror 
  </div>
  <div class="form-group">
    <label for="exampleFormControlSelect1">Active</label>
    <select class="form-control" name="active" id="active">
      <option value="1">yes</option>
      <option value="0">no</option>
    </select>
  </div>
  <div class="form-group">
  <label for="exampleFormControlSelect1">Category</label>
    <select class="form-control"  name="cat_id" id="cat_id">
    @foreach($categories as $category)
    @if($category->parent_id != 0)
      <option value="{{$category->id}}">{{$category->name}}</option>
      @endif
      @endforeach
    </select>
    @error('cat_id')
    <div class="mt-4 mb-4"><span class="alert alert-danger">{{$message}}</span></div>
    @enderror
  </div>
  <div class="form-group">
  <label for="exampleFormControlSelect1">Auther</label>
    <select class="form-control"  name="auther_id" id="auther_id">
    @foreach($authers as $auther)
      <option value="{{$auther->id}}">{{$auther->name}}</option>
      @endforeach
    </select>
    @error('auther_id')
    <div class="mt-4 mb-4"><span class="alert alert-danger">{{$message}}</span></div>
    @enderror
  </div>

</div>
<div class="modal-footer">
<input type="hidden" name="blog_id" id="blog_id" value="">
<input type="hidden" name="button_action" id="button_action" value="insert">
<input type="submit" name="submit" id="action" value="add" class="btn btn-info">
<button type="button" class="close" data-dismis="modal">close</button>

</div>


</form>


</div>
</div>
@endsection
@section('scripts')

<script src="{{asset('public/js/blogs.js')}}"></script> 
@endsection