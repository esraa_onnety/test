<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Categories</title>
    <link rel="stylesheet" type="text/css" href="{{asset('public/css/bootstrap.min.css')}}"/>  
    <link rel="stylesheet" href="{{asset('public/css/jquery.dataTables.css')}}">
    
  
    <style>
      .error{
        color:red;
        font-style:italic;
    }
  
  th{
    text-align:center;
  }
  
  </style>
   
</head>
<body>

<div class="container">
@yield('content')
</div>


<script src="{{asset('public/js/jquery-3.4.1.min.js')}}"></script>
<script src="{{asset('public/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/js/jquery.validate.min.js')}}"></script>
<script src="{{asset('public/js/additional-methods.min.js')}}"></script>
<script src="{{asset('public/js/bootstrap.min.js')}}"></script>
<script>
  var config = {
    url: "{{ url('/admin') }}",
    src:"{{asset('')}}"
  
  }

</script>


@yield('scripts')


</body>
</html>