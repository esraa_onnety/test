@extends('admin.master')
@section('content')
{{ csrf_field() }} 
<h1 class="text-center mb-4 mt-2">Categories</h1>
@if (session('message'))
<div class="alert alert-success alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ session('message') }}</strong>
</div>
@endif

<input type="hidden" name="p_id" id="p_id" value="">
<div class="row">
  <div class="col-3 mb-5">
  <div class="row  d-flex justify-content-center">
  @include('admin.sidebar')
  </div>
  <div class="col-9">
    <div id="msg"></div>
  <button name="add" type="buttn" class="btn btn-success btn-sm mb-5" id="add_data">ADD</button>
<table  class="table table-bordered DataTable" style="width:100%"  id="cat_table">

<thead>
<th>Category Name</th>
<th>Icon</th>
<th>parent Category</th>
<th>actions</th>
</thead>
</tr>

<table class="table table-bordered DataTable"></table>



</table>
</div>
</div>
</div>
<div id="catModel" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<form action="" method="post" id="cat_form">

<div class="modal-header">

<button type="button" class="close" data-dismis="modal">&times;</button>
<h4 class="modal-title">Add Data</h4>

</div>
<div class="modal-body">@csrf
<div class="form-group ">
    <label>Category Name</label>
    <input type="text" class="form-control"  aria-describedby="emailHelp" placeholder="Enter name" name="name" id="name">
    @error('name')
    <div class="mt-4 mb-4"><span class="alert alert-danger">{{$message}}</span></div>
    @enderror
    
  </div>
  <div class="form-group">
  <div class="custom-file">
    <input type="file" name="icon" id="icon" class="custom-file-input">
    <div id="image-holder" class="row mt-2 justify-content-center"> </div>
    @error('image')
    <div class="mt-4 mb-4"><span class="alert alert-danger">{{$message}}</span></div>
    @enderror
    <label class="custom-file-label">icon</label>
    </div>
  </div>
  <div class="form-group">
  <label for="exampleFormControlSelect1">Parent Category</label>
    <select class="form-control"  name="category" id="category">
    <option value="0"></option>
    @foreach($cats as $cat)
    @if($cat->parent_id==0)
      <option value="{{$cat->id}}">{{$cat->name}}</option>
      @endif
      @endforeach
    </select>
  </div>
  
  
<div class="modal-footer">
<input type="hidden" name="cat_id" id="cat_id" value="">
<input type="hidden" name="button_action" id="button_action" value="insert">
<input type="submit" name="submit" id="action" value="add" class="btn btn-info">
<button type="button" class="close" data-dismis="modal">close</button>

</div>


</form>


</div>
</div>
</div>
<div id="subCat" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<form action="">
<ul>


<li> </li>

</ul>
</form>

</div>
</div>
@endsection
@section('scripts')
<script>
  var neWconfig = {
    parent_id: "{{ $id }}"
  }

</script>
<script src="{{asset('/public/js/category.js')}}"></script>
@endsection
