@extends('admin.master')
@section('content')

<h1 class="text-center mb-4 mt-2">Authers</h1>
@if (session('message'))
<div class="alert alert-success alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ session('message') }}</strong>
</div>
@endif
<div class="row">
  <div class="col-3"><div class="row  d-flex justify-content-center">
     @include('admin.sidebar')
            </div>
  <div class="col-9">
  <div id="msg"></div>
<button name="add" type="buttn" class="btn btn-success btn-sm mb-5" id="add_data">ADD</button>
<table class="table table-bordered DataTable" style="width:100%" id="auther_table" >
<tr>
<thead>
<th>Auther name</th>
<th>Image</th>
<th>action</th>
</thead>
</tr>




</table>
</div>
</div>
</div>
<div id="autherModel" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<form action="{{route('authers.store')}}" method="post" id="auther_form" enctype="multipart/form-data">

<div class="modal-header">

<button type="button" class="close" data-dismis="modal">&times;</button>
<h4 class="modal-title">Add Data</h4>

</div>
<div class="modal-body">@csrf

          <div class="alert alert-danger print-error-msg" style="display:none;"></div>
<div class="form-group ">
    <label>Auther Name</label>
    <input type="text" class="form-control"  aria-describedby="emailHelp" placeholder="Enterthe auther name" name="name" id="name">
    @error('name')
    <div class="mt-4 mb-4"><span class="alert alert-danger">{{$message}}</span></div>
    @enderror
    
  </div>
  <div class="form-group">
  <div class="custom-file">
    
    <input type="file" name="image" id="image" class="custom-file-input"> 
    <div id="image-holder" class="row mt-2 justify-content-center"> </div>
    @error('image')
    <div class="mt-4 mb-4"><span class="alert alert-danger">{{$message}}</span></div>
    @enderror
    <label class="custom-file-label">Image</label>
    </div>
  </div>
  


</div>
<div class="modal-footer">
<input type="hidden" name="auther_id" id="auther_id" value="">

<input type="hidden" name="button_action" id="button_action" value="insert">
<input type="submit" name="submit" id="action" value="add" class="btn btn-info">
<button type="button" class="close" data-dismis="modal">close</button>

</div>


</form>


</div>
</div>
@endsection
@section('scripts')

<script src="{{asset('public/js/auther.js')}}"></script>
@endsection