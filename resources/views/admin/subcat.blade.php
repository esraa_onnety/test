<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}"/>  
    <link rel="stylesheet" href="{{asset('css/jquery.dataTables.css')}}">
    
  

   
</head>
<body>

<div class="container">
<h1 class="text-center mb-4 mt-2">Categories</h1>
<button name="add" type="buttn" class="btn btn-success btn-sm mb-5" id="add_data">ADD</button>
<input type="hidden" name="p_id" id="p_id" value="">
<table id="cat_table" class="table table-bordered DataTable" style="width:100%">
<tr>
<th>Category Name</th>
<th>Icon</th>
<th>action</th>
</tr>
<tr>


</table>
</div>
<div id="catModel" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<form action="" method="post" id="cat_form">

<div class="modal-header">

<button type="button" class="close" data-dismis="modal">&times;</button>
<h4 class="modal-title">Add Data</h4>

</div>
<div class="modal-body">@csrf
<div class="form-group ">
    <label>Category Name</label>
    <input type="text" class="form-control"  aria-describedby="emailHelp" placeholder="Enter name" name="name" id="name">
    
  </div>
  <div class="form-group">
    <label>Icon</label>
    <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="Enter icon" name="icon" id="icon">
    
  </div>
  
  
<div class="modal-footer">
<input type="hidden" name="cat_id" id="cat_id" value="">
<input type="hidden" name="button_action" id="button_action" value="insert">
<input type="submit" name="submit" id="action" value="add" class="btn btn-info">
<button type="button" class="close" data-dismis="modal">close</button>

</div>


</form>


</div>
</div>
</div>
<div id="subCat" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<form action="">
<ul>


<li> </li>

</ul>
</form>

</div>
</div>
</div>


















<script src="js/jquery-3.4.1.min.js"></script>
<script src="js/jquery.dataTables.min.js"></script>
<script src="js/bootstrap.min.js"></script>

<script> 
$(document).ready(function(){
$('#cat_table').DataTable({

"processing": true,
"serverSide": true,
"ajax":"{{route('cat_getdata')}}",
"columns":[

    {"data":"name"},
    {"data":"icon"},
    {"data":"parent_id"},
    {"data":"action", orderable:false,searchable:false}
    
]



});
$('#add_data').click(function(){

$('#catModel').modal('show');
$('#cat_form')[0].reset();
$('#form_output').html('');
$('#button_action').val('insert');

$('#action').val('add');

});
$('#cat_form').on('submit',function(event){

event.preventDefault();
var form_data=$(this).serialize();
$.ajax({


    url:"{{route('cat_postdata')}}",
    method:"POST",
    data:form_data,
    dataType:"json",
  
    
});

});
$(document).on('click','.edit',function(){
  var id = $(this).attr("id");
  $.ajax({
        url:"{{route('cat_actiondata')}}",
        method:'get',
        data:{id:id},
        dataType:'json',
        success:function(data){

          $('#name').val(data.name);
          $('#icon').val(data.icon);
          $('#parent_id').val(data.parent_id);
          $('#cat_id').val(id);
          $('#catModel').modal('show');
          $('#action').val('edit');
          $('.modal-title').text('Edit Data');
          $('#button_action').val('update');

        }


  })
});

  $(document).on('click','.delete',function(){
    var id = $(this).attr("id");
    if(confirm("are you sure you want to delete this?"))
   {
     $.ajax({
         url:"{{route('cat_removedata')}}",
         method:"get",
         data:{id:id},
         success:function(data){
           alert(data);
           $('#cat_table').DataTable().ajax.reload();
         }
     })
   }
   else{
     return false;
   }
  });

  $('.close').on('click',function(){
    $('#catModel').modal('hide');
  });

});
</script>

</body>
</html>