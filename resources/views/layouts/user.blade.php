 @extends('layouts.auth')

    @section('content')
   
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card" style="background-color:grey;">
                    <div class="card-header" style="background-color:#D0D0D0;">Blogs</div>

             <div class="card-body mx-auto">
                    <div>
                    <form  method="get" id="blog_filter">
                    @csrf
                    <select class="custom-select" name="category" id="category">
                    <option value=""></option>
                    @foreach($categories as $cat)
                    
                             <option value="{{$cat->id}}">{{$cat->name}}</option> 
                
                             @endforeach
                     </select>
                     </form></div>
                    @foreach($blogs as $blog)
                    <div class="card mb-4 mt-4" style="width: 18rem;">
                    <img class="card-img-top" src="public/uploads/blogs/{{$blog->image}}" alt="Card image cap">
                 <div class="card-body">
                 <h5 class="card-title"><span class="mr-2 text-info">Title:</span>{{$blog->title}}</h5>
                 <p class="card-text"><span class="mr-2 text-info ">Description:</span>{{$blog->description}}</p>
                  </div>
               <ul class="list-group list-group-flush">
               <li class="list-group-item"><span class="mr-2 text-info">Auther:</span>{{$blog->auther_name}}</li>
               <li class="list-group-item"><span class="mr-2 text-info">Category:</span>{{$blog->cat_name}}</li>
               <div class="row d-flex justify-content-center"><span class="mr-1 text-info">Views:{{$blog->views_count}}</span></div>
              
                </ul>
  
</div>

@endforeach
<nav aria-label="..." class="mx-auto">
  <ul class="pagination">
    
   <li>{{$blogs->Links()}}</li>
  </ul>
</nav>

                    </div>
                </div>
            </div>
        </div>
    </div>


    @endsection
  