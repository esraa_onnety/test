@extends('layouts.auth')

    @section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        Hi boss!
                    <ul>
                    <li><a href="{{route('authers.index')}}">Authers</a></li>
                    <li><a href="{{route('blogs.index')}}">Blogs</a></li>
                    <li><a href="{{route('categories.index')}}">Categories</a></li>
                    </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection