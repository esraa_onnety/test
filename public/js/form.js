$(document).ready(function(){
    $.validator.addMethod("noSpace",function(value,element){
        return value=='' ||value.trim().length !=0
    },"spaces are not allowed");
  
$('#register').validate({
      rules:{
               email:{
                   required:true,
                   email:true
               },
               password:"required",
               password2:{
                       required:true,
                       equalTo:"#password"
               },
               first_name:{
                   required:true,
                   nowhitespace:true,
                   lettersonly:true,
                   noSpace:true
               },
               last_name:{
                required:true,
                nowhitespace:true,
                lettersonly:true
            },
            agree:{
                required:true,
            },
            options:{
                required:true,
            }

      },
      messages:{
        email:{
            required:"please enter your email",
            email:"please enter a <em>valid</em> email"
        },
        agree:{
            required:"you should be agree first"
        }


      },
      errorPlacement:function(error,element){
          if(element.is(":radio")){

            element.appendTo(element.parent(".gender"));
          }
          else if(element.is(":checkbox")){
            element.appendTo(element.parent(".agree"));
          }
          else{
              error.insertAfter(element);
          }
      }





});






});