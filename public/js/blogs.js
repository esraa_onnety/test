$(document).ready(function(){
    $('#blog_table').DataTable({
    "processing":true,
    "serverSide":true,
    "ajax": {
      "url": config.url+'/blogs/data',
      "type": "POST",
      data: { _token: $('input[name="_token"]').val() },
  },
    "columns":[
    
        {"data":"title"},
        {"data":"description"},
        {"data":"image"},
        {"data":"order"},
        {"data":"active"},
        {"data":"views_count"},
        {"data":"cat_name"},
        {"data":"auther_name"},
        {"data":"action", orderable:false,searchable:false}
        
    ],
    columnDefs: [
        { targets: 2,
          render: function(data) {
            return '<img src="'+config.src+'/public/uploads/blogs/'+data+'" width="42" height="42">'
          }
        }   
      ]
    
    
    });
    $('#add_data').click(function(){
    
    $('#dataModel').modal('show');
    $('#data_form')[0].reset();
    $('#form_output').html('');
    $('#button_action').val('insert');
    $('#action').val('add');
    
    });
    $('#data_form').on('submit',function(event){
    
    event.preventDefault();
    var input_hidden=$('#blog_id').val();
    var formData = new FormData($(this)[0]);
    var action = config.url+'/blogs';
    if(input_hidden !=''){
      action = config.url+'/blogs/'+input_hidden;
      formData.append("_method","put");
    }
    
    $.ajax({
    
    
       url:action,
       method:"POST",
       data:formData,
       dataType:'JSON',
       contentType: false,
       cache: false,
       processData: false,
       success:function(data){
        $('#msg').html('<div class="alert alert-success col-ssm-12" >' + data.message + '</div>');
        $('#dataModel').modal('hide');
        $('#blog_table').DataTable().ajax.reload();

       },
       errors:{
        "title": [
            "The title field is required."
        ],
        "description": [
            "The description field is required."
        ],
        "image": [
          "The image field is required."
      ],
      "order": [
          "The order field is required."
      ],
      "active": [
        "The active field is required."
    ],
    "cat_id": [
        "The category field is required."
    ],
    "auther_id": [
      "The auther field is required."
  ]
    }
       
    });
    
    });
    $("#image").on('change', function () {

      if (typeof (FileReader) != "undefined") {
  
          var image_holder = $("#image-holder");
          image_holder.empty();
  
          var reader = new FileReader();
          reader.onload = function (e) {
              $("<img />", {
                  "src": e.target.result,
                  "class": "thumb-image",
                  "height":"100",
                  "width":"100"
              }).appendTo(image_holder);
  
          }
          image_holder.show();
          reader.readAsDataURL($(this)[0].files[0]);
      } else {
          alert("This browser does not support FileReader.");
      }
  });
    $(document).on('click','.edit',function(){
      var id = $(this).attr("id");
      $.ajax({
            url:config.url+'/blogs/'+id+'/edit',
            method:'get',
            data:{id:id},
            dataType:'json',
            success:function(data){
    
              $('#title').val(data.title);
              $('#description').val(data.description);
              $('#image').val();
              $('#order').val(data.order);
              $('#active').val(data.active);
              $('#cat_id').val(data.cat_id);
              $('#auther_id').val(data.auther_id);
              $('#blog_id').val(id);
              $('#dataModel').modal('show');
              $('#action').val('edit');
              $('.modal-title').text('Edit Data');
              $('#button_action').val('update');
    
            }
    
    
      })
    });
    $(document).on('click','.delete',function(){
        var id = $(this).attr("id");
        if(confirm("are you sure you want to delete this?"))
       {
         $.ajax({
             url:config.url+'/blogs/'+id,
             method:"post",
             data:{id:id,_token:$('input[name="_token"]').val(),_method:"delete"},
             success:function(data){
               alert(data);
               $('#blog_table').DataTable().ajax.reload();
             }
         })
       }
       else{
         return false;
       }
      });
      $('.close').on('click',function(){
        $('#dataModel').modal('hide');
      });
      $('#data_form').validate({
        rules:{
                
                   title:{
                       required:true,
                      
                   },
                   description:{
                    required:true
                    
                },
                   image:{
                    required:true,
                    extension: "jpg|jpeg|png|gif"
              
                },
                order:{
                  required:true
                  
              },
              active:{
                required:true
                
            },
            cat_id:{
              required:true
              
          },
          auther_id:{
            required:true
            
        },
          },
          messages:{
            title:{
                required:"please enter the title",
                title:"please enter a <em>valid</em> name"
            },
            image:{
                required:"please insert photo",
                extension: 'Not an image!'
            }
            
    
          }
    
    
      });
      
    
    
    
    
    });
    
    
    