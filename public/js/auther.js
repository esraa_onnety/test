
$(document).ready(function(){
   $('#auther_table').DataTable({
  
  //"processing": true,
  "serverSide": true,
  "ajax":{
    "url":config.url+'/authers/data',
  "type":"post",
  data: { _token: $('input[name="_token"]').val() }
},
  "columns":[
  
      {"data":"name"},
      {"data":"image"},
      {"data":"action", orderable:false,searchable:false}
      
  ],
  columnDefs: [
      { targets: 1,
        render: function(data) {
          return '<img src="'+config.src+'/public/uploads/authers/'+data+'" width="42" height="42">'
        }
      }   
    ]
  
  
  
  });
  $('#add_data').click(function(){
  
  $('#autherModel').modal('show');
  $('#auther_form')[0].reset();
  $('#form_output').html('');
  $('#button_action').val('insert');
  
  $('#action').val('add');
  
  });
  $('#auther_form').on('submit',function(event){
  
  event.preventDefault();
  var input_hidden=$('#auther_id').val();
  var formData = new FormData($(this)[0]);
  var action = config.url+'/authers';
  if(input_hidden !=''){
    action = config.url+'/authers/'+input_hidden;
    formData.append("_method","put");
  }
  
  
    $.ajax({
  url:action,
  method:"POST",
  data:formData,
  dataType:'JSON',
  contentType: false,
  cache: false,
  processData: false,
  success: function(data) {
  $('#msg').html('<div class="alert alert-success col-ssm-12" >' + data.message + '</div>');
    $('#autherModel').modal('hide');
    $('#auther_table').DataTable().ajax.reload();             
              },
        errors:{
          "name": [
              "The name field is required."
          ],
          "image": [
              "The image field is required."
          ]
      }
  
  });
   });
   $("#image").on('change', function () {

    if (typeof (FileReader) != "undefined") {

        var image_holder = $("#image-holder");
        image_holder.empty();

        var reader = new FileReader();
        reader.onload = function (e) {
            $("<img />", {
                "src": e.target.result,
                "class": "thumb-image",
                "height":"100",
                "width":"100"
            }).appendTo(image_holder);

        }
        image_holder.show();
        reader.readAsDataURL($(this)[0].files[0]);
    } else {
        alert("This browser does not support FileReader.");
    }
});
  $(document).on('click','.edit',function(){
    var id = $(this).attr("id");
    $.ajax({
          url:config.url+'/authers/'+id+'/edit',
          method:'get',
          data:{id:id},
          dataType:'json',
          success:function(data){
  
            $('#name').val(data.name);
            $('#image').val();
            $('#auther_id').val(id);
            $('#autherModel').modal('show');
            $('#action').val('edit');
            $('.modal-title').text('Edit Data');
            $('#button_action').val('update');
  
          }
  
  
    })
  });
    $(document).on('click','.delete',function(){
      var id = $(this).attr("id");
      if(confirm("are you sure you want to delete this?"))
     {
       $.ajax({
           url:config.url+'/authers/'+id,
           method:"post",
           data:{id:id,_token:$('input[name="_token"]').val(),_method:"delete"},
           success:function(data){
             alert(data);
             $('#auther_table').DataTable().ajax.reload();
           },
           error: function(xhr, textStatus, errorThrown) {
                      console.log(xhr);
                  }
       })
     }
     else{
       return false;
     }
    });
    $('#action').on('click',function(){
     
      
    });
    $('.close').on('click',function(){
      $('#autherModel').modal('hide');
    });
    $('#auther_form').validate({
      rules:{
              
                 name:{
                     required:true,
                     nowhitespace:true,
                     lettersonly:true
                     
                 },
                 image:{
                  required:true,
                  extension: "jpg|jpeg|png|gif"
            
              },
            
        },
        messages:{
          name:{
              required:"please enter the Auther Name",
              name:"please enter a <em>valid</em> name"
          },
          image:{
              required:"please insert photo",
              extension: 'Not an image!'
          }
          
  
        }
  
  
    });
  
    
  });