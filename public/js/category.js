
$(document).ready(function(){
    $('#cat_table').DataTable({
    
    "processing": true,
    "serverSide": true,
   
            "ajax": {
                "url": config.url+'/categories/data',
                "type": "POST",
                data: { parent_id: neWconfig.parent_id, _token: $('input[name="_token"]').val() },
            },
   
   
    "columns":[
    
        {"data":"name"},
        {"data":"icon"},
        {"data":"parent_id"},
        {"data":"action", orderable:false,searchable:false}
        
    ],
    columnDefs: [
        { targets: 1,
          render: function(data) {
            return '<img src="'+config.src+'/public/uploads/categories/'+data+'" width="42" height="42">'
          }
        },
        { targets: 2,
          render: function(data) {
            return '<p>'+data+'</p>'
          }
        }
          
      ]
    
    
    
    });
    $('#add_data').click(function(){
    
    $('#catModel').modal('show');
    $('#cat_form')[0].reset();
    $('#form_output').html('');
    $('#button_action').val('insert');
    
    $('#action').val('add');
    
    });
    $('#cat_form').on('submit',function(event){
    
    event.preventDefault();
    var input_hidden=$('#cat_id').val();
  var formData = new FormData($(this)[0]);
  var action = config.url+'/categories';
  if(input_hidden !=''){
    action = config.url+'/categories/'+input_hidden;
    formData.append("_method","put");
  }
    $.ajax({
    
    
        url:action,
        method:"POST",
       data:formData,
       dataType:'JSON',
       contentType: false,
       cache: false,
       processData: false,
       success: function(data){ // response is returned by server
        $('#msg').html('<div class="alert alert-success col-ssm-12" >' + data.message + '</div>');
    $('#catModel').modal('hide');
    $('#cat_table').DataTable().ajax.reload();  // <------ Here you add rows updated on table
    },
    errors:{
      "name": [
          "The name field is required."
      ],
      "icon": [
          "The image field is required."
      ]
  } 
   
        
        
    });
    
    });
    $("#icon").on('change', function () {

      if (typeof (FileReader) != "undefined") {
  
          var image_holder = $("#image-holder");
          image_holder.empty();
  
          var reader = new FileReader();
          reader.onload = function (e) {
              $("<img />", {
                  "src": e.target.result,
                  "class": "thumb-image",
                  "height":"100",
                  "width":"100"
              }).appendTo(image_holder);
  
          }
          image_holder.show();
          reader.readAsDataURL($(this)[0].files[0]);
      } else {
          alert("This browser does not support FileReader.");
      }
  });
    $(document).on('click','.edit',function(){
      var id = $(this).attr("id");
      $.ajax({
            url:config.url+'/categories/'+id+'/edit',
            method:'get',
            data:{id:id},
            dataType:'json',
            success:function(data){
    
              $('#name').val(data.name);
              $('#icon').val();
              $('#category').val(data.id);
              $('#cat_id').val(id);
              $('#catModel').modal('show');
              $('#action').val('edit');
              $('.modal-title').text('Edit Data');
              $('#button_action').val('update');
    
            }
    
    
      })
    });
  
      $(document).on('click','.delete',function(){
        var id = $(this).attr("id");
        if(confirm("are you sure you want to delete this?"))
       {
         $.ajax({
             url:config.url+'/categories/'+id,
             method:"post",
             data:{id:id,_token:$('input[name="_token"]').val(),_method:"delete"},
             success:function(data){
              alert(data);
              $('#catModel').modal('hide');
              $('#cat_table').DataTable().ajax.reload();
            }
         })
       }
       else{
         return false;
       }
      });
      $('#action').on('click',function(){
        $('#catModel').modal('hide');
        $('#cat_table').DataTable().ajax.reload();
      });
      
      $('.close').on('click',function(){
        $('#catModel').modal('hide');
      });
      $('#cat_form').validate({
        rules:{
                
                   name:{
                       required:true,
                      
                       
                   },
                   icon:{
                    required:true,
                    extension: "jpg|jpeg|png|gif"
              
                },
              
          },
          messages:{
            name:{
                required:"please enter the category Name",
                name:"please enter a <em>valid</em> name"
            },
            icon:{
                required:"please insert photo",
                extension: 'Not an image!'
            }
            
    
          }
    
    
      });
    
    });