<?php


namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Models\Category;
use Validator;
use File;
use DB;

class CategoryController extends Controller
{
    private $rules =[
        'name' => 'required',
        'icon' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
    
        $cats=Category::get();
        $id=$request->input('id') ? $request->input('id') : 0;
        return view('admin.category',compact('cats','id'));
    }
    
    public function data(Request $request){
      
        $parent_id = $request->input('parent_id');
        $cat=Category::select(['id','name','icon','parent_id'])
        ->where('parent_id',$parent_id);
    
        return Datatables::of($cat)->addColumn('action', function($cat){
    
            return '<a class ="btn btn-sm btn-primary edit" id="'.$cat->id.'"><i class="glyphicon glyphicon-edit"></i>Edit</a>
            <a class ="btn btn-sm btn-danger delete ml-1" id="'.$cat->id.'"><i class="glyphicon glyphicon-remove"></i>Delete</a>
            <a  href="/category/admin/categories?id='.$cat->id.'" name="check" class ="btn btn-sm btn-success subcat ml-1" id="'.$cat->id.'"><i class="glyphicon glyphicon-remove"></i>sub_category</a>';
    
    
        })->editColumn('parent_id', 'Parent:{{$parent_id}}!')->setRowAttr(['align' => 'center'])->make(true);
    
    
        }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function upload(Request $request){

        $file=$request->file('icon');
        $extension = $file->getClientOriginalExtension();
        $filename = time() . '.' .$extension;
        $file->move('public/uploads/categories',$filename);
        return $filename;

    }
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try{
        $validator = Validator::make(request()->all(),$this->rules+=['icon' =>'required']);
        if(!$validator->passes()){
             if($request->ajax()){
                return response()->json(array(
                    'errors' => $validator->getMessageBag()->toArray()
            
                ), 400);
                   
             }
             else{
                return response()->json(['error'=>$validator->errors()->all()]);
             }
        }
                  $cat= new Category();
                  $cat->name=$request->get('name');
                  $cat->parent_id=$request->get('category');
                    
                    $cat->icon=$this->upload($request);
                    $cat->save();
                if($request->ajax()){
                    return response()->json([
                        'message' => 'Data inserted successfully!',
                   ]);
                    
                }
                else{
                    return redirect()->route('categories.index')->with('message',' Data inserted successfully!');
                }
            }
            catch(\Exception $e){
                return $e->getMessage();
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try{
     
        $cat=Category::find($id);
        if($cat){
        $output =array(
            'name'=> $cat->name,
            'icon'=> $cat->icon,
            'id'=>$cat->parent_id
        );
        echo json_encode($output);
    }
}
    catch(\Exception $e){
        return $e->getMessage();
    }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    $validator = Validator::make(request()->all(),$this->rules);
        if(!$validator->passes()){
             if($request->ajax()){
                return response()->json(array(
                    'errors' => $validator->getMessageBag()->toArray()
            
                ), 400);
                   
             }
             else{
                return  redirect()->back()->withErrors($validator)->withInput();
             }
        }
    
        $cat=Category::find($request->get('cat_id'));
        if($cat){
        $cat->name=$request->get('name');
        $cat->parent_id=$request->get('category');
        if($request->hasfile('icon')){
          $old_image= $cat->icon;
          File::delete('public/uploads/categories/'.$cat->icon);
           $cat->icon=$this->upload($request);
       }
       else{
           return $request;
           $cat->icon='';
       }
        $cat->save();
        if($request->ajax()){
            return response()->json([
                'message' => 'Data updated successfully!',
           ]);
            
        }
        else{
            return redirect()->route('categories.index')->with('message',' Data updated successfully!');
        }
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        //
        try{
        $cat=Category::find($request->input('id'));
        if($cat){
            $cat->delete();
            if(File::exists('public/uploads/categories/'.$cat->icon)){
            File::delete('public/uploads/categories/'.$cat->icon);
            }
            echo 'data deleted';
        }
    }
    catch(\Exception $e){
        return $e->getMessage();
    }
              
    }
}
