<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Auther;
use Validator;
use File;

use Yajra\Datatables\Datatables;

class AutherController extends Controller
{
    private $rules =[
        'name' => 'required',
        'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
    ];
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.auther');
    }

    public function data()
    {
        //
        try{
            $authers=Auther::select('id','name','image');
            return Datatables::of($authers)
            ->addColumn('action', function($authers){
                return '<a class ="btn btn-sm btn-primary edit" id="'.$authers->id.'"><i class="glyphicon glyphicon-edit"></i>Edit</a>
                <a class ="btn btn-sm btn-danger delete ml-1" id="'.$authers->id.'"><i class="glyphicon glyphicon-remove"></i>Delete</a>';
            })
            ->setRowAttr(['align' => 'center'])
            ->make(true);
        }
        catch(\Exception $e){
            return $e->getMessage();
       }
      
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function upload(Request $request){

        $file=$request->file('image');
        $extension = $file->getClientOriginalExtension();
        $filename = time() . '.' .$extension;
        $file->move('public/uploads/authers',$filename);
        return $filename;
    }
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try{
        $validator = Validator::make(request()->all(),$this->rules+=['image' => 'required']);
        if(!$validator->passes()){
             if($request->ajax()){
                return response()->json(array(
                    'errors' => $validator->getMessageBag()->toArray()
            
                ), 400);
             }
             else{
                return  redirect()->back()->withErrors($validator)->withInput();
             }
        }
                $auther= new Auther();
                $auther->name=$request->input('name');
                $auther->image=$this->upload($request);
                $auther->save();
           
            if($request->ajax()){
                return response()->json([
                    'message' => 'Data inserted successfully!',
               ]);
            }
            else{
                return redirect()->route('authers.index')->with('message',' Data inserted successfully!');
            }
        }
        catch(\Exception $e){
            return $e->getMessage();
       }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        
         $auther=Auther::find($id);
         if($auther){
         $output =array(
             'name'=> $auther->name,
             'image'=> $auther->image
         );
         echo json_encode($output);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        //
        $validator = Validator::make(request()->all(),$this->rules);
        if(!$validator->passes()){
             if($request->ajax()){
                return response()->json(array(
                    'errors' => $validator->getMessageBag()->toArray()
            
                ), 400);
             }
             else{
                return  redirect()->back()->withErrors($validator)->withInput();
             }
        }
        $auther=Auther::find($id);
        if($auther){
        $auther->name=$request->get('name');
        if($request->hasfile('image')){
         $old_image= $auther->image;
         if(File::exists('public/uploads/authers/'.$auther->image)){
            File::delete('public/uploads/authers/'.$auther->image);
            }
          $auther->image=$this->upload($request);;
      }
      
        $auther->save();
        if($request->ajax()){
            return response()->json([
                'message' => 'Data updated successfully!',
           ]);
        }
        else{
            return redirect()->route('authers.index')->with('message',' Data updated successfully!');
        }
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        try{
            $auther=Auther::find($request->input('id'));
            if($auther){
                $auther->delete();
                if(File::exists('public/uploads/authers/'.$auther->image)){
                File::delete('public/uploads/authers/'.$auther->image);
                }
                echo 'data deleted';
            } 
               
        }
        catch(\Exception $e){
            return $e->getMessage();
        }
       
    }
}
