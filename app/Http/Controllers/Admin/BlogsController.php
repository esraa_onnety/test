<?php


namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Validator;
use DB;
use App\Models\Blog;
use App\Models\Auther;
use App\Models\Category;
use File;

class BlogsController extends Controller
{
    private $rules =[
        'title' => 'required',
        'description' => 'required',
        'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        'order' => 'required',
        'active' => 'required',
        'cat_id' => 'required',
        'auther_id' => 'required',
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $authers=Auther::get();
        $categories=Category::get();
        return view('admin.blog',compact('authers','categories'));
    }
    public function data(){
        $blog= DB::table('blogs')
        ->join('categories', 'categories.id', '=', 'blogs.cat_id')
        ->join('authers', 'authers.id', '=', 'blogs.auther_id')
        ->select('blogs.id','blogs.title','blogs.description','blogs.image','blogs.order','blogs.active','blogs.views_count','categories.name as cat_name','authers.name as auther_name')
        ->get();
        return Datatables::of($blog)->addColumn('action',function($blog){
            return '<a class ="btn btn-sm btn-primary edit mb-2" id="'.$blog->id.'"><i class="glyphicon glyphicon-edit"></i>Edit</a><a class ="btn btn-sm btn-danger delete ml-1" id="'.$blog->id.'"><i class="glyphicon glyphicon-remove"></i>Delete</a>';
        })->setRowAttr(['align' => 'center'])->make(true);
    
    
        }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function upload(Request $request){
    
        $file=$request->file('image');
        $extension = $file->getClientOriginalExtension();
        $filename = time() . '.' .$extension;
        $file->move('public/uploads/blogs',$filename);
        return $filename;

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
        $validator = Validator::make(request()->all(),$this->rules+=['image' => 'required']);
        if(!$validator->passes()){
             if($request->ajax()){
                return response()->json(array(
                    'errors' => $validator->getMessageBag()->toArray()
            
                ), 400);
             }
             else{
                return  redirect()->back()->withErrors($validator)->withInput();;
             }
        }
        
        $blog= new Blog();
        $blog->title=$request->get('title');
        $blog->description=$request->get('description');
       
        $blog->image=$this->upload($request);
        $blog->order=$request->get('order');
        $blog->active=$request->get('active');
        $blog->cat_id=$request->get('cat_id');
        $blog->auther_id=$request->get('auther_id');
        $blog->save();
        if($request->ajax()){
            return response()->json([
                'message' => 'Data inserted successfully!',
           ]);
        }
        else{
            return redirect()->route('blogs.index')->with('message',' Data inserted successfully!');
        }
    }
    catch(\Exception $e){
        return $e->getMessage();
    }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try{
      
        $blog=Blog::find($id);
        if($blog){
        $output =array(
            'title'=> $blog->title,
            'description'=> $blog->description,
            'image'=> $blog->image,
            'order'=> $blog->order,
            'active'=> $blog->active,
            'cat_id'=> $blog->cat_id,
            'auther_id'=> $blog->auther_id
        );
        echo json_encode($output);
    }
}
catch(\Exception $e){
    return $e->getMessage();
}
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    $validator = Validator::make(request()->all(),$this->rules);
    if(!$validator->passes()){
         if($request->ajax()){
            return response()->json(array(
                'errors' => $validator->getMessageBag()->toArray()
        
            ), 400);
         }
         else{
            return redirect()->route('blogs.index')->with('message',' Data updated successfully!');
         }
    }
    
        
        $blog=Blog::find($request->get('blog_id'));
        if($blog){
        $blog->title=$request->get('title');
        $blog->description=$request->get('description');
        if($request->hasfile('image')){
        $old_image= $blog->image;
        if(File::exists('public/uploads/blogs/'.$blog->image)){
         File::delete('public/uploads/blogs/'.$blog->image);
        }
        $blog->image=$this->upload($request);
        }
        $blog->order=$request->get('order');
        $blog->active=$request->get('active');
        $blog->cat_id=$request->get('cat_id');
        $blog->auther_id=$request->get('auther_id');
        $blog->save();
        if($request->ajax()){
            return response()->json([
                'message' => 'Data updated successfully!',
           ]);
        }
        else{
            return redirect()->route('blogs.index')->with('message',' Data updated successfully!');;
        }
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        try{
        $blog=Blog::find($request->input('id'));
        if($blog){
            $blog->delete();
            if(File::exists('public/uploads/blogs/'.$blog->image)){
            File::delete('public/uploads/blogs/'.$blog->image);
            }
            echo 'data deleted';
        }
        
    }
    catch(\Exception $e){
        return $e->getMessage();
    }
}

}
