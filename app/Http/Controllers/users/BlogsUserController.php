<?php

namespace App\Http\Controllers\users;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\LengthAwarePaginator;
use DB;
use App\Models\Blog;
use App\Models\Auther;
use App\Models\Category;

class BlogsUserController extends Controller
{
    //
    
    public function index(Request $request){
     
     $categories=Category::select('id','name')
     ->where('parent_id','<>',0)
     ->get();
     if($request->input('category')!=''){
      $blog_id=$request->input('category');
      $blogs= DB::table('blogs')
      ->join('categories', 'categories.id', '=', 'blogs.cat_id')
      ->join('authers', 'authers.id', '=', 'blogs.auther_id')
      ->select('blogs.id','blogs.title','blogs.description','blogs.image','blogs.order','blogs.active','blogs.views_count','categories.name as cat_name','authers.name as auther_name')
      ->where('blogs.active',1)
      ->where('blogs.cat_it','=',$blog_id)
      ->paginate(1);
    }else{
     $blogs= Blog::join('categories', 'categories.id', '=', 'blogs.cat_id')
    ->join('authers', 'authers.id', '=', 'blogs.auther_id')
    ->select('blogs.id','blogs.title','blogs.description','blogs.image','blogs.order','blogs.active','blogs.views_count','categories.name as cat_name','authers.name as auther_name')
    ->where('blogs.active',1)
    ->paginate(1);
  }
        return view('layouts.user',compact('blogs','categories'));
    }
    
  
    public function show($id){
      try
      {
        $views = Blog::find($id);
        if(!$views){
            
        }
        $views->increment('views_count');
       
        return view('layouts.show', compact('views'));
       
    }
    catch(\Exception $e){
      dd($e->getMessage());
    }
}
}
