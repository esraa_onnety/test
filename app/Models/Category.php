<?php

namespace App\Models;
use File;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    protected $fillable=['name' ,'icon','parent_id'];
    
    public function parent(){

        return $this->belongsTo('Category','parent_id');
   
       }
       public function children(){
   
           return $this->hasMany('Category','parent_id');
       }
       
       public function blogs(){
           return $this->hasMany('App\Models\Blog');
       }
       public static function boot()
       {
           parent::boot();
           
          
           Static::deleted(function($category)
           {   
               
                if(File::exists('public/images/'.$category->icon)){
                    File::delete('public/images/'.$category->icon);
                }
                   
              
           });
       }

    
}
