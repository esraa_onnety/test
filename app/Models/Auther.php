<?php

namespace App\Models;
use File;

use Illuminate\Database\Eloquent\Model;

class Auther extends Model
{
    //
    protected $fillable=['name','image'];
    public function blogs(){
        return $this->hasMany('App\Models\Blog');
    }
    
    public static function boot()
    {
        parent::boot();
        
       
        Static::deleted(function($auther)
        {   
            
             if(File::exists('public/images/'.$auther->image)){
                 File::delete('public/images/'.$auther->image);
             }
                
           
        });
    }
    

    
}
