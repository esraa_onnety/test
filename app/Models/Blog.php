<?php

namespace App\Models;
use File;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
protected $fillable=['title' ,'decription','image','order','active','cat_id','auther_id'];
    public function categories(){

        return $this->belongsTo('App\Models\Category','cat_id');
  }
  public function authers(){

      return $this->belongTo('App\Models\Auther','auther_id');

  }
  public static function boot()
  {
      parent::boot();
      
     
      Static::deleted(function($blog)
      {   
          
           if(File::exists('public/images/'.$blog->image)){
               File::delete('public/images/'.$blog->image);
           }
              
         
      });
  }
}
