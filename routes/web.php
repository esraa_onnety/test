<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Auth::routes();
//auth
Route::group(['middleware' => 'auth:admin', 'prefix' => 'admin', 'namespace' => 'Admin'],function(){  
Route::get('/', 'AdminController@index');
//Route::view('/admin', 'layouts.admin');
//blogs
Route::resource('blogs','BlogsController');
Route::post('blogs/data','BlogsController@data');
Route::post('blogs/upload','BlogsController@upload');
//categories
Route::resource('categories','CategoryController');
Route::post('/categories/data','CategoryController@data');
Route::post('/categories/upload','CategoryController@upload');
//authers
Route::resource('authers','AutherController');
Route::post('/authers/data','AutherController@data');
Route::post('/authers/upload','AutherController@upload');
 
});
    Route::get('/login/admin', 'Auth\LoginController@showAdminLoginForm');
    Route::get('/login/user', 'Auth\LoginController@showuserLoginForm');
    Route::get('/register/admin', 'Auth\RegisterController@showAdminRegisterForm');
    Route::get('/register/user', 'Auth\RegisterController@showuserRegisterForm');

    Route::post('/login/admin', 'Auth\LoginController@adminLogin');
    Route::post('/login/user', 'Auth\LoginController@userLogin');
    Route::post('/register/admin', 'Auth\RegisterController@createAdmin');
    Route::post('/register/user', 'Auth\RegisterController@createuser');

    //Route::view('/home', 'home')->middleware('auth');
    Route::get('home','HomeController@index')->middleware('auth');

    //home for users
    Route::get('/blogs', 'users\BlogsUserController@index')->name('user');
    Route::get('/blogs/{id}', 'users\BlogsUserController@show')->name('views');
   
   


